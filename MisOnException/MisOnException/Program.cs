﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnException
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("anna kaks arvu");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            try
            {
                Console.WriteLine(a / b);
            }
            catch (DivideByZeroException e)
            {

                Console.WriteLine("ära jaga nulliga");
                throw;
            }
            catch (FormatException e) //see veel ei toimi - kontrolli üle
            {

                Console.WriteLine("vale formaat");
            }

            catch (Exception e)
            {
                Console.WriteLine("miski läks valesti");
                Console.WriteLine(e.GetType().Name);
                Console.WriteLine(e.Message);

            }
            finally //see blokk täidetakse vaatamata sellele, kas juhtus midagi või eik
            {
               Console.WriteLine("see tuleb igal juhul teha");
            }
        }

    }

   
}
