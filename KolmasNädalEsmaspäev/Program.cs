﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Data.SqlClient;

namespace KolmasNädalEsmaspäev
{
    partial class Employee

    {
        public string FullName => FirstName + " " + LastName;

     }
    class Program
    {
        static void Main(string[] args)
        {
            northwindEntities ne = new northwindEntities();
            ne.Products
                .Where(x => x.UnitPrice < 50)
                .Where(x => x.UnitPrice > 10)
                .Where(x => !x.Discontinued)
                .Select(x => new { x.ProductName, x.UnitPrice })
                .ToList()
                .ForEach(x => Console.WriteLine($"Toode: {x.ProductName} \thinnaga {x.UnitPrice}"));

            foreach (var e in ne.Employees)
            Console.WriteLine(e.FullName);
            Console.WriteLine($"Töötaja: {e.FullName} ülemus on {e.Employee1?. FullName ?? "Jumal"}");

            if (ne.Products is IEnumerable<Product>)
                Console.WriteLine("on küll");
            else
                Console.WriteLine("ega ei ole");

        }
    }
}
